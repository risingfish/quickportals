package QuickPortals;

import QuickPortals.Entity.Portal;
import QuickPortals.Utils.PortalFileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by work on 10/25/15.
 */
public class PortalManager {
    private List<Portal> portals;
    private static final Logger logger = Bukkit.getLogger();
    private static PortalManager pmanager = null;

	/**
	 * PortalManager constructor - singleton
	 * @return {@link PortalManager}
	 */
    public static PortalManager getInstance() {
        if (pmanager == null) {
            pmanager = new PortalManager();
        }

        return pmanager;
    }

    /**
     * Basic PortalManager constructor
     */
    private PortalManager() {
        this.portals = new ArrayList<>();
    }

    /**
     * Load portals. THis method will replace the current list of portals with a new one.
     * @param portals
     */
    public void setPortals(List<Portal> portals) {
        this.portals = portals;
    }


    /**
     * Add a new portal to the list
     * @param portal - {@link QuickPortals.Entity.Portal}
     */
    public void addPortal(Portal portal) {
	    if(null == portal) {
		    logger.log(Level.WARNING, "Adding a portal failed. Portal was null");
		    return;
	    }

	    try {
		    PortalFileUtils.getInstance().savePortal(portal);
		    portals.add(portal);
		    logger.log(Level.INFO, "Adding a portal");
	    } catch (IOException e) {
		    logger.log(Level.SEVERE, "Adding a portal failed. IOException while saving portal ", e);
	    }
    }

    /**
     * Get portal for a specific block. Returns a Portal object or null if there is no portal at the given block.
     * @param block - {@link org.bukkit.block.Block}
     * @return {@link QuickPortals.Entity.Portal}
     */
    public Portal getPortalByBlock(Block block) {
	    Portal lookedUpPortal = null;

	    if(null != block && null != block.getLocation()) {
		    for (Portal portal : portals) {
			    //TODO: This is not matching when the X,Y,Z coords are all the same.
			    if (portal.getLocation().equals(block.getLocation())) {
				    lookedUpPortal = portal;
			    }
		    }
	    }else {
		    logger.log(Level.WARNING, "Block passed to getPortalByBlock was either null or had no location, param was: ", block);
	    }

        return lookedUpPortal;
    }

	/**
	 *
	 * @param portalName {@code java.lang.String} portalName
	 * @return {@link Portal}
	 */
	public Portal getPortalByName(String portalName) {
		Portal lookedUpPortal = null;

		if(null != portalName) {
			for (Portal portal : portals) {
				if (portalName.trim().equals(portal.getName())) {
					lookedUpPortal = portal;
				}
			}
		}else {
			logger.log(Level.WARNING, "Target portal's name was null on getPortalByName lookup: ");
		}

		return lookedUpPortal;
	}
}
