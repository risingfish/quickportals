package QuickPortals.Utils;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.bukkit.Location;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * Created by work on 10/27/15.
 */
public class LocationSerializer implements JsonSerializer<Location> {
    @Override
    public JsonElement serialize(Location location, Type type, JsonSerializationContext jsonSerializationContext) {
        Type typeOfHashMap = new TypeToken<Map<String, Object>>() { }.getType();
        Gson gson = new GsonBuilder().create();
        return gson.toJsonTree(location.serialize(), typeOfHashMap);
    }
}
