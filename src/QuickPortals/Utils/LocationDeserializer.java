package QuickPortals.Utils;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.bukkit.Location;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * Created by work on 10/27/15.
 */
public class LocationDeserializer implements JsonDeserializer<Location> {
    @Override
    public Location deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        Type typeOfHashMap = new TypeToken<Map<String, Object>>() { }.getType();
        Gson gson = new GsonBuilder().create();
        return Location.deserialize(gson.fromJson(jsonElement, typeOfHashMap));
    }
}
