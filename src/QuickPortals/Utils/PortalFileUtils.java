package QuickPortals.Utils;

import QuickPortals.Entity.Portal;
import QuickPortals.Main;
import com.avaje.ebean.EbeanServer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by work on 10/26/15.
 */
public class PortalFileUtils {
    private static PortalFileUtils instance;
    private static GsonBuilder gson;

    private FileConfiguration config;
    private Logger logger;
    private File dataFolder;
    private String prefix = "portal-";

    private File dataFile;
    private File portalData;


    /**
     * Get config file
     * @return Returns a FileConfiguration object
     */
    public FileConfiguration getConfig() {
        return config;
    }

    /**
     * Get the current instance of this control
     * @return
     */
    public static PortalFileUtils getInstance() {
        return PortalFileUtils.instance;
    }

    /**
     * Initialize the control
     * @param main {@link QuickPortals.Main}
     * @return {@link PortalFileUtils}
     */
    public static PortalFileUtils init(Main main) {
        if (PortalFileUtils.instance == null) {
            PortalFileUtils.gson = new GsonBuilder();
            gson.registerTypeAdapter(Location.class, new LocationSerializer());
            gson.registerTypeAdapter(Location.class, new LocationDeserializer());

            PortalFileUtils.instance = new PortalFileUtils(main);
        }

        return PortalFileUtils.instance;
    }

	/**
	 * Private constructor - singleton
	 * @param main {@link QuickPortals.Main}
	 */
    private PortalFileUtils(Main main) {
        this.config = main.getConfig();
        this.logger = main.getLogger();
        this.dataFolder = main.getDataFolder();
        this.setupDataFile();
    }

	/**
	 * Persist a portal
	 * @param portal {@link QuickPortals.Entity.Portal}
	 */
    public void savePortal(Portal portal) throws IOException {
        Gson gson = PortalFileUtils.gson.create();
        String pSerial = gson.toJson(portal);

        File file = new File(this.portalData, this.prefix + portal.toString() + ".json");

        if (!file.exists()) {
            if (file.createNewFile()) {

                file.setWritable(true);
            }

            FileWriter writer = new FileWriter(file);
            writer.write(pSerial.toCharArray());
            writer.close();
        }

    }

    public ArrayList<Portal> loadPortals() {
        ArrayList<Portal> portals = new ArrayList<>();
        Gson gson = PortalFileUtils.gson.create();

        File[] listOfFiles = this.portalData.listFiles();
        ArrayList<File> deleteFiles = new ArrayList<>();

        if (listOfFiles != null) {
	        for (File listOfFile : listOfFiles) {
		        if (listOfFile.isFile()) {
			        byte[] encoded = new byte[0];
			        Portal port = null;

			        try {
				        encoded = Files.readAllBytes(listOfFile.toPath());
			        } catch (IOException e) {
				        logger.log(Level.WARNING, "Error reading portal gson data", e);
			        }

			        //Converts file to Portal class
			        String json = new String(encoded, Charset.forName("UTF-8"));
			        try {
				        port = gson.fromJson(json, Portal.class);
			        }catch (JsonSyntaxException jex) {
				        logger.log(Level.WARNING, "Error parsing signs created prior to target being Portal instead of String", jex);
			        }

			        if(null != port) {
				        Material matType = port.getLocation().getWorld().getBlockAt(port.getLocation()).getType();

				        //Verifying that location is still a sign
				        if (matType.equals(Material.SIGN_POST) || matType.equals(Material.SIGN) || matType.equals(Material.WALL_SIGN)) {
					        logger.info("Re-hydrating the portal " + port.toString());
					        portals.add(port);
				        } else {
					        deleteFiles.add(listOfFile);
				        }
			        }
		        }
	        }

            if (deleteFiles.size() > 0) {
                for (File file : deleteFiles) {
                    logger.info("Deleting the file " + file.getAbsolutePath() + " because the portal does not exist.");
                    file.delete();
                }
            }
        }

        return portals;
    }

    private void setupDataFile() {
        try {
            if (!this.dataFolder.exists()) {
                this.dataFolder.mkdirs();
            }
            this.portalData = new File(this.dataFolder, "portals");
            if (!this.portalData.exists()) {
                Bukkit.getLogger().info("Portal data folder not found, creating it now.");
                this.portalData.mkdirs();
            } else {
                Bukkit.getLogger().info("Portal data folder found.");
            }
        } catch (Exception e) {
	        logger.log(Level.SEVERE, "Error setting up plugin data file/folder structure: ", e);
            e.printStackTrace();

        }

    }

    public void deletePortal(Portal portal) {
        File file = new File(this.portalData, this.prefix + portal.toString() + ".json");

        if (file.exists()) {
            if (!file.delete()) {
                logger.severe("Could not delete file " + file.getAbsolutePath());
            }
        }
    }
}
