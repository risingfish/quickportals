package QuickPortals.EventHandler;

import QuickPortals.Entity.Portal;
import QuickPortals.Parser.SignParser;
import QuickPortals.PortalManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.craftbukkit.v1_8_R3.block.CraftBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * On {@link org.bukkit.event.player.PlayerInteractEvent} checks that the player is interacting with a sign.
 * Then handles portal signs if they are the ones clicked.
 */
public class SignClickHandler implements Listener {
	private static final Logger logger = Bukkit.getLogger();
	private PortalManager portalManager = PortalManager.getInstance();
	private SignParser parser = SignParser.getInstance();

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
	    //If action is right click on block
	    if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
		    Block block = e.getClickedBlock();
		    //If block is not null
		    if (block != null) {
			    Material blockType = block.getType();
			    //Only process if the player interacted with a sign
			    if (blockType.equals(Material.SIGN_POST) || blockType.equals(Material.SIGN) || blockType.equals(Material.WALL_SIGN)) {
					//Get portal based on the block attached to the sign
				    Portal portal = PortalManager.getInstance().getPortalByBlock(block);
				    Player player = e.getPlayer();

				    if (portal != null) {
					    Portal target = portal.getTargetPortal();

					    //Read sign again to determine if portal has been placed since creation with target name
					    if(target == null) {
						    BlockState state = block.getState();
						    Sign sign = (Sign) state;
						    String targetPortalName = parser.getTargetPortalName(sign.getLines());
						    if(null != targetPortalName) {
							    target = portalManager.getPortalByName(targetPortalName);
						    }
					    }

					    if(null != target) {
						    //If portal has a valid target portal
						    org.bukkit.material.Sign sign = new org.bukkit.material.Sign(block.getType());
						    BlockFace face;
						    Location tpLocation;

						    face = sign.getFacing();
						    //TODO: Should there be a check to see if the space is clear so the player doesn't suffocate
						    if (null != face) {
							    //Try to do teleport with facing information
							    tpLocation = target.getLocation().clone().add(face.getModX(), face.getModY(), face.getModZ());
						    } else {
							    //Otherwise just teleport
							    tpLocation = player.getWorld().getBlockAt(target.getLocation()).getLocation();
						    }

						    player.sendMessage(ChatColor.YELLOW + "Beaming...");
						    player.teleport(tpLocation);
						    logger.log(Level.INFO, "Player " + player.getDisplayName() + " successfully teleported to " + tpLocation.toString());
						    e.setCancelled(true);
					    } else {
						    player.sendMessage(ChatColor.YELLOW + "This portal is an exit only.");
					    }
				    }
			    }
		    }
	    }
	    //TODO: Else if left click destroy then lookup sign and remove from data file
    }
}
