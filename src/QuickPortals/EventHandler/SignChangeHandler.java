package QuickPortals.EventHandler;

import QuickPortals.Entity.Portal;
import QuickPortals.Parser.SignParser;
import QuickPortals.PortalManager;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

public class SignChangeHandler implements Listener {
	private static PortalManager portalManager = PortalManager.getInstance();

    @EventHandler
    public void onSignChange(SignChangeEvent e) {
        Block block = e.getBlock();
	    SignParser parser = SignParser.getInstance();
	    Portal port = new Portal(block.getLocation());

        //Set name - can be null
	    port.setName(parser.getPortalName(e));
	    //Set target name - can be null
	    Portal target = portalManager.getPortalByName(parser.getTargetPortalName(e));
	    port.setTargetPortal(target);

	    //Save portal and log to player
        PortalManager.getInstance().addPortal(port);
        Player player = e.getPlayer();
        player.sendMessage("Portal placed");

    }
}
