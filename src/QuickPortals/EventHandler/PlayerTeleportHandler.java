package QuickPortals.EventHandler;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;

/**
 * @author Grant Mills
 * @since 11/15/15
 */
public class PlayerTeleportHandler implements Listener {

	@EventHandler
	public void onTeleport(PlayerTeleportEvent e) {
		Player player = e.getPlayer();
		//From and to portals should sparkle
		Location sparkleFromLoc = player.getLocation();
		Location sparkleLoc = e.getTo();

		World world = sparkleLoc.getWorld();

		//Over time
		for(int i = 500; i > 0; i--) {
			//Swirl this around the player.
			world.playEffect(sparkleLoc, Effect.PORTAL, 100);
			world.playEffect(sparkleFromLoc, Effect.PORTAL, 100);
			world.playEffect(sparkleLoc, Effect.PARTICLE_SMOKE, 1500);
			world.playEffect(sparkleFromLoc, Effect.PARTICLE_SMOKE, 1500);
		}
		// Play Enderman teleport sound
		world.playEffect(sparkleLoc, Effect.STEP_SOUND, 100);
		world.playSound(sparkleLoc, Sound.ENDERMAN_TELEPORT, 100, 1);

	}
}
