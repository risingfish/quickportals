package QuickPortals.EventHandler;

import QuickPortals.Entity.Portal;
import QuickPortals.PortalManager;
import QuickPortals.Utils.PortalFileUtils;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * Created by zac on 10/26/15.
 */
public class BlockBreakHandler implements Listener {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e){
        Block block = e.getBlock();

        Portal portal = PortalManager.getInstance().getPortalByBlock(block);

        if (portal != null) {
            PortalFileUtils.getInstance().deletePortal(portal);
        }
    }

}
