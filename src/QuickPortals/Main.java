package QuickPortals;

import QuickPortals.Entity.Portal;
import QuickPortals.EventHandler.*;
import QuickPortals.Utils.PortalFileUtils;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by work on 10/23/15.
 */
public class Main extends JavaPlugin {
    private File dataFile;

    @Override
    public void onLoad() {
        PortalManager.getInstance();
        this.createConfig();
    }

    @Override
    public void onEnable() {
        PluginDescriptionFile pdffile = getDescription();
        Logger logger = getLogger();
        logger.info(pdffile.getName() + " has been enabled (V." + pdffile.getVersion() + ")");

        PortalFileUtils.init(this);

        ArrayList<Portal> portals = PortalFileUtils.getInstance().loadPortals();
        PortalManager.getInstance().setPortals(portals);

        PluginManager pm = this.getServer().getPluginManager();
        pm.registerEvents(new SignChangeHandler(), this);
        pm.registerEvents(new SignClickHandler(), this);
        pm.registerEvents(new BlockBreakHandler(), this);
        pm.registerEvents(new BlockPlacedHandler(), this);
        pm.registerEvents(new PlayerTeleportHandler(), this);

        this.saveConfig();
    }

    @Override
    public void onDisable() {

        this.saveConfig();

        PluginDescriptionFile pdffile = getDescription();
        Logger logger = getLogger();
        logger.severe("GOOD BYE WORLD");

        logger.info(pdffile.getName() + " has been enabled (V." + pdffile.getVersion() + ")");
    }

    private void createConfig() {
        try {
            if (!getDataFolder().exists()) {
                getDataFolder().mkdirs();
            }
            File file = new File(getDataFolder(), "config.yml");
            if (!file.exists()) {
                Bukkit.getLogger().info("Config.yml not found, creating!");
                saveDefaultConfig();
            } else {
                getLogger().info("Config.yml found, loading!");
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }
}
