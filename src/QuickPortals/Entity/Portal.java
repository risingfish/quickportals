package QuickPortals.Entity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by work on 10/25/15.
 */
public class Portal implements ConfigurationSerializable {

    private Location location;
    private String name;
    private Portal targetPortal;

    /**
     * Get block this portal is in.
     * @return org.bukkit.block.Block
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Set block this portal is in.
     * @param location Set the block this portal is in.
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * Get the targetPortal endpoint for this portal
     * @return
     */
    public Portal getTargetPortal() {
        return targetPortal;
    }

    /**
     * Set the targetPortal endpoint for this portal
     * @param targetPortal
     */
    public void setTargetPortal(Portal targetPortal) {
        this.targetPortal = targetPortal;
    }

    /**
     * Gets the name of a given portal
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of a given portal
     * @param name
     */
    public void setName(String name) {
	    this.name = null;
        if(null != name) {
	        this.name = name.trim();
        }
    }

    /**
     * Basic constructor for JSON serialization
     */
    public Portal() {
    }

    /**
     * Create a portal. This constructor will assign a random UUID so it should only be used for a new portal endpoint.
     * @param location
     */
    public Portal(Location location) {
        this.location = location;
    }

    /**
     * Check if a portal is an exit only.
     * @return
     */
    public boolean isExitOnly() {
        return (this.targetPortal == null && this.name != null);
    }

    /**
     * Check if a portal is an entry only
     * @return
     */
    public boolean isEntryOnly() {
        return (this.name == null && this.targetPortal != null);
    }

    /**
     * Check if a portal is complete (is an entry point and has a target exit)
     * @return
     */
    public boolean isComplete() {
        return (this.name != null && this.targetPortal != null);
    }


    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("location", this.location.serialize());
        data.put("name", this.name);
        data.put("targetPortal", this.targetPortal);

        return data;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.location.getWorld().getName());
        builder.append("x");
        builder.append(this.location.getX());
        builder.append("y");
        builder.append(this.location.getY());
        builder.append("z");
        builder.append(this.location.getZ());
        return builder.toString();
    }

    /**
     * Deserialize a portal object. this is required for the ConfigurationSerializable interface
     * @param map Serialized data
     * @return Portal object
     */
    public static Portal deserialize(Map<String, Object> map) {
        Location loc = Location.deserialize((Map<String, Object>) map.get("location"));
        loc.setWorld(Bukkit.getServer().getWorld(loc.getWorld().getName()));

        Portal portal = new Portal(loc);

        portal.setName((String) map.get("name"));
	    //TODO: Refactor for Portal type. Loop over portals
        portal.setTargetPortal((Portal) map.get("targetPortal"));

        return portal;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Portal)) {
            return false;
        } else if (obj == this) {
            return true;
        }

        Portal p = (Portal)obj;
        return  (p.location.equals(this.location) && p.name.equals(this.name) && p.targetPortal.equals(this.targetPortal));
    }

    @Override
    public int hashCode() {
        int hash = 10;
        hash = 23 * hash + (this.name.hashCode());
        hash = 23 * hash + (this.targetPortal.hashCode());
        hash = 23 * hash + this.location.hashCode();
        return hash;
    }
}
