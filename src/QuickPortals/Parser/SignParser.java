package QuickPortals.Parser;
import QuickPortals.Entity.Portal;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.event.block.SignChangeEvent;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by work on 10/25/15.
 */
public class SignParser {
	public static final String PORTAL_NAME_DELIMITER = "N:";
	public static final String TARGET_PORTAL_DELIMETER = "T:";
	private static SignParser instance;
    private static final Logger logger = Bukkit.getLogger();

	/**
	 * Get sign parser instance - singleton
	 * @return
	 */
    public static SignParser getInstance() {
        if (instance == null) {
            SignParser.instance = new SignParser();
        }
        return SignParser.instance;
    }

	/**
	 * Private constructor - singleton
	 */
    private SignParser() {}

	private Boolean hasLines(SignChangeEvent e) {
		if(e.getLines().length > 0) {
			return true;
		}
		logger.log(Level.INFO, "Sign event has no lines. May not be sign.");
		return false;
	}

	/**
	 * Searchs for portal name and target delimeters and returns portal name
	 * @param e {@link org.bukkit.event.block.SignChangeEvent}
	 * @param searchParam delimeter to search for
	 * @return
	 */
	private String searchPortalName(SignChangeEvent e, String searchParam) {
		String name = null;
		if(hasLines(e)) {
			name = searchPortalName(e.getLines(), searchParam);
		}
		return name;
	}

	private String searchPortalName(String[] lines, String searchParam) {
		String name = null;
		for (String line : lines) {

			logger.info("Parsing line: [" + line + "]");

			if (line.contains(searchParam.toLowerCase())) {
				String[] mr = line.split(searchParam.toLowerCase());
				name = mr[1].trim();
			}
		}
		return name;
	}

	public String getPortalName(SignChangeEvent e) {
		return searchPortalName(e, PORTAL_NAME_DELIMITER);
	}

	public String getTargetPortalName(SignChangeEvent e) {
		return searchPortalName(e, TARGET_PORTAL_DELIMETER);
	}

	public String getTargetPortalName(String[] lines) {
		return searchPortalName(lines, TARGET_PORTAL_DELIMETER);
	}

	public String getPortalName(String[] lines) {
		return searchPortalName(lines, PORTAL_NAME_DELIMITER);
	}
}
