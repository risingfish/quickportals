#Quick Portals
##v1.0

Quick portals is a Minecraft plugin inspired buy the Sign Portals plugin
at [http://dev.bukkit.org/bukkit-plugins/portal-no-need-running-so-much/].
While it worked fairly well, it has some bugs, and I felt the way to setup
portals was a little to verbose.

To use quickportals you will need ot be running [https://www.spigotmc.org/].
Put the plugin in your plugins folder and restart the server.

Once you are in the game, create a new sign. To create a portal you need to
have one or both of the following lines in the sign:


    N:name
    T:target


* N represents the name of the portal, and allows other portals to use it as an exit.
* T represents the portal the portal links to, it can target any portal with a name.

The exact line number these lines are on do not matter.